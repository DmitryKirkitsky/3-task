package dao;

import entity.SchoolgroupsEntity;
import interfaces.DAOInterface;
import utils.HibernateSessionFactoryUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

public class SchoolgroupsDAO implements DAOInterface<SchoolgroupsEntity> {
    private Session session;
    @Override
    public void save(SchoolgroupsEntity schoolgroupsEntity) {
        session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.persist(schoolgroupsEntity);
        transaction.commit();
    }

    @Override
    public void remove(SchoolgroupsEntity schoolgroupsEntity) {
        session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.remove(schoolgroupsEntity);
        transaction.commit();
    }

    @Override
    public void update(SchoolgroupsEntity schoolgroupsEntity) {
        session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.update(schoolgroupsEntity);
        transaction.commit();
    }

    @Override
    public List<SchoolgroupsEntity> getAll() {
        session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        List<SchoolgroupsEntity> result = session.createQuery("from SchoolgroupsEntity").list();
        session.close();
        return result;
    }

    @Override
    public SchoolgroupsEntity getById(int id) {
        SchoolgroupsEntity schoolgroupsEntity;
        session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        schoolgroupsEntity = session.get(SchoolgroupsEntity.class, id);
        session.close();
        return schoolgroupsEntity;
    }
}
