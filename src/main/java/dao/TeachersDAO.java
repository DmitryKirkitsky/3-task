package dao;

import entity.TeachersEntity;
import interfaces.DAOInterface;
import utils.HibernateSessionFactoryUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

public class TeachersDAO implements DAOInterface<TeachersEntity> {
    Session session;
    @Override
    public void save(TeachersEntity teachersEntity) {
        session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.persist(teachersEntity);
        transaction.commit();
    }

    @Override
    public void remove(TeachersEntity teachersEntity) {
        session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.remove(teachersEntity);
        transaction.commit();
    }

    @Override
    public void update(TeachersEntity teachersEntity) {
        session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.update(teachersEntity);
        transaction.commit();
    }

    @Override
    public List<TeachersEntity> getAll() {
        session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        List<TeachersEntity> result = session.createQuery("from TeachersEntity").list();
        session.close();
        return result;
    }

    @Override
    public TeachersEntity getById(int id) {
        TeachersEntity teachersEntity;
        session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        teachersEntity = session.get(TeachersEntity.class, id);
        session.close();
        return teachersEntity;
    }
}
