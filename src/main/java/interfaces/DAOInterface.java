package interfaces;

import java.util.List;

public interface DAOInterface<T> {
    void save(T t);
    void remove(T t);
    void update(T t);
    List<T> getAll();
    T getById(int id);
}
