package viewcontroller;

import dao.*;
import entity.*;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Scanner;
import java.text.SimpleDateFormat;

public class ViewController {
    private LessonsDAO lessonsDAO = new LessonsDAO();
    private PeoplesDAO peoplesDAO = new PeoplesDAO();
    private PeoplesprogressDAO peoplesprogressDAO = new PeoplesprogressDAO();
    private ScheduleDAO scheduleDAO = new ScheduleDAO();
    private SchoolgroupsDAO schoolgroupsDAO = new SchoolgroupsDAO();
    private TeachersDAO teachersDAO = new TeachersDAO();
    private SchoolgroupsTeachersDAO schoolgroupsTeachersDAO = new SchoolgroupsTeachersDAO();
    public void showMainScreen() {
        System.out.println("Press 1 to show table");
        System.out.println("Press 2 to delete record");
        System.out.println("Press 3 to add record");
        System.out.println("Press 4 to update record");
    }
    public static int getNumberFromUser() {
        Scanner scanner = new Scanner(System.in);
        if (scanner.hasNextInt()) {
            return scanner.nextInt();
        }
        return 0;
    }
    public boolean isWantToContinue() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Do you wanna continue? (y/n)");
        if (scanner.hasNext()) {
            return scanner.next().equals("y");
        }
        return false;
    }

    public void showTableList() {
        System.out.println("1 - Lessons");
        System.out.println("2 - Peoples");
        System.out.println("3 - Teachers");
        System.out.println("4 - Schedule");
        System.out.println("5 - Schoolgroups");
        System.out.println("6 - PeoplesProgress");
    }

    public void processNumberFromUser(int userInput) {
        switch (userInput) {
            case 1:
                showTableList();
                processOfShowingSelectedTable(getNumberFromUser());
                break;
            case 2:
                showTableList();
                processOfRemovingTable(getNumberFromUser());
                break;
            case 3:
                showTableList();
                processOfAddingSelectedTable(getNumberFromUser());
                break;
            case 4:
                showTableList();
                processOfUpdatingSelectedTable(getNumberFromUser());
                break;
            default:
                System.out.println("error");
        }
    }

    public void showList(List<?> objects) {
        objects.forEach(System.out::println);
    }

    public void processOfShowingSelectedTable(int selectedTable) {
        switch(selectedTable) {
            case 1:
                showList(lessonsDAO.getAll());
                break;
            case 2:
                showList(peoplesDAO.getAll());
                break;
            case 3:
                showList(teachersDAO.getAll());
                break;
            case 4:
                showList(scheduleDAO.getAll());
                break;
            case 5:
                showList(schoolgroupsDAO.getAll());
                break;
            case 6:
                showList(peoplesprogressDAO.getAll());
                break;
            default:
                System.out.println("Error. Invalid index");
                break;
        }
    }

    public void processOfRemovingTable(int userInput) {
        switch(userInput) {
            case 1:
                showList(lessonsDAO.getAll());
                lessonsDAO.remove(lessonsDAO.getById(getNumberFromUser()));
                break;
            case 2:
                showList(peoplesDAO.getAll());
                peoplesDAO.remove(peoplesDAO.getById(getNumberFromUser()));
                break;
            case 3:
                showList(teachersDAO.getAll());
                teachersDAO.remove(teachersDAO.getById(getNumberFromUser()));
                break;
            case 4:
                showList(scheduleDAO.getAll());
                scheduleDAO.remove(scheduleDAO.getById(getNumberFromUser()));
                break;
            case 5:
                showList(schoolgroupsDAO.getAll());
                schoolgroupsDAO.remove(schoolgroupsDAO.getById(getNumberFromUser()));
                break;
            case 6:
                showList(peoplesprogressDAO.getAll());
                peoplesprogressDAO.remove(peoplesprogressDAO.getById(getNumberFromUser()));
                break;
            default:
                System.out.println("Invalid number of table");
                break;
        }
    }

    public void processOfAddingSelectedTable(int selectedTable) {
        switch(selectedTable) {
            case 1:
                lessonsDAO.save(getLessonFromUser(null));
                break;
            case 2:
                peoplesDAO.save(getPeopleFromUser(null));
                break;
            case 3:
                teachersDAO.save(getTeachersFromUser(null));
                break;
            case 4:
                scheduleDAO.save(getScheduleFromUser(null));
                break;
            case 5:
                schoolgroupsDAO.save(getSchoolGroupFromUser(null));
                break;
            case 6:
                peoplesprogressDAO.save(getPeopleProgressFromUser(null));
                break;
            default:
                System.out.println("Invalid number of table");
                break;
        }
    }

    public void processOfUpdatingSelectedTable(int userInput) {
        switch(userInput) {
            case 1:
                showList(lessonsDAO.getAll());
                lessonsDAO.update(getLessonFromUser(getNumberFromUser()));
                break;
            case 2:
                showList(peoplesDAO.getAll());
                peoplesDAO.update(getPeopleFromUser(getNumberFromUser()));
                break;
            case 3:
                showList(teachersDAO.getAll());
                teachersDAO.update(getTeachersFromUser(getNumberFromUser()));
                break;
            case 4:
                showList(scheduleDAO.getAll());
                scheduleDAO.update(getScheduleFromUser(getNumberFromUser()));
                break;
            case 5:
                showList(schoolgroupsDAO.getAll());
                schoolgroupsDAO.update(getSchoolGroupFromUser(getNumberFromUser()));
                break;
            case 6:
                showList(peoplesprogressDAO.getAll());
                peoplesprogressDAO.update(getPeopleProgressFromUser(getNumberFromUser()));
                break;
            default:
                System.out.println("Invalid number of table");
                break;
        }
    }

    public String getStringFromUser() {
        Scanner scanner = new Scanner(System.in);
        if(scanner.hasNext()) {
            return scanner.nextLine();
        }
        return null;
    }

    public LessonsEntity getLessonFromUser(Integer id) {
        String lessonsname;
        System.out.println("Input lessons name");
        lessonsname = getStringFromUser();
        return new LessonsEntity(id, lessonsname);
    }

    public java.sql.Date stringToDate(String stringDate) throws ParseException {
        DateFormat format = new SimpleDateFormat("yyyy-mm-dd");
        Date date;
        date = format.parse(stringDate);
        java.sql.Date newDate = new java.sql.Date(date.getTime());
        return newDate;
    }

    public PeoplesEntity getPeopleFromUser(Integer id) {
        String fio, date;
        System.out.println("Input people fio");
        fio = getStringFromUser();
        System.out.println("Input people birthday YYYY-MM-DD (for example 2000-05-20)");
        date = getStringFromUser();
        java.sql.Date birthDate = stringToDate(date);
        return new PeoplesEntity(id, fio, birthDate);
    }

    public TeachersEntity getTeachersFromUser(Integer id) {
        String fio, date,levelEducation;
        Integer yearsExperience;
        System.out.println("Input teachers FIO");
        fio = getStringFromUser();
        System.out.println("Input teachers birthday YYYY-MM-DD (for example 2000-05-20)");
        date = getStringFromUser();
        System.out.println("Input teachers years of experience");
        yearsExperience = getNumberFromUser();
        System.out.println("Input teachers level of education");
        levelEducation = getStringFromUser();
        java.sql.Date birthDate = stringToDate(date);
        return new TeachersEntity(id, fio, birthDate, yearsExperience, levelEducation);
    }

    public ScheduleEntity getScheduleFromUser(Integer id) {
        int lessonsNumber;
        String weekDay;
        LessonsEntity lessons;
        SchoolgroupsTeachersEntity schoolgroupsTeachers;
        PeoplesprogressEntity peoplesprogress;
        System.out.println("Input week day");
        weekDay = getStringFromUser();
        System.out.println("Input number of lessons (max 7)");
        lessonsNumber = getNumberFromUser();
        return new ScheduleEntity(id, weekDay, lessonsNumber);
    }

    public SchoolgroupsEntity getSchoolGroupFromUser(Integer id) {
        Integer peoplesAmount, headman;
        String groupName;
        System.out.println("Input peoples amount");
        peoplesAmount = getNumberFromUser();
        System.out.println("Input headman id of group");
        headman = getNumberFromUser();
        System.out.println("Input group name");
        groupName = getStringFromUser();
        return new SchoolgroupsEntity(id, peoplesAmount, headman, groupName);
    }

    public PeoplesprogressEntity getPeopleProgressFromUser(Integer id) {
        Integer averageScore, passesAmount;
        System.out.println("Input peoples average score");
        averageScore = getNumberFromUser();
        System.out.println("Input peoples passes");
        passesAmount = getNumberFromUser();
        return new PeoplesprogressEntity(id, averageScore, passesAmount);
    }

}
