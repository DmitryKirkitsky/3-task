package entity;

import lombok.*;

import javax.persistence.*;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode
@Entity
@Table(name = "schoolgroups", schema = "schoolmain")
public class SchoolgroupsEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    @Column(name = "peoplesamount", nullable = true)
    private Integer peoplesamount;
    @Column(name = "headman_id", nullable = true)
    private Integer headman_id;
    @Column(name = "groupname", nullable = true, length = 5)
    private String groupname;


}
