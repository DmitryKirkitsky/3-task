package entity;

import lombok.*;

import javax.persistence.*;
import java.sql.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode
@Entity
@Table(name = "teachers", schema = "schoolmain")
public class TeachersEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    @Column(name = "teacherfio", nullable = true, length = 40)
    private String teacherfio;
    @Column(name = "birhday", nullable = true)
    private Date birhday;
    @Column(name = "yearexperience", nullable = true)
    private Integer yearexperience;
    @Column(name = "leveleducation", nullable = true, length = 10)
    private String leveleducation;

}
