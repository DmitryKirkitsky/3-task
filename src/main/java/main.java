import viewcontroller.ViewController;

public class main {
    public static void main(String[] args) {
        try{
            ViewController viewController = new ViewController();
            do {
                viewController.showMainScreen();
                viewController.processNumberFromUser(viewController.getNumberFromUser());
            } while(viewController.isWantToContinue());
        }
        catch(Exception ex){
            System.out.println("Connection failed...");
            System.out.println(ex);
        }
    }
}
